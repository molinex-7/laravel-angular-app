# Laravel Angular App

Made by Thiago Molina with technologies like laravel angular and docker

### Install

First of all install docker, update de database

	$ sudo apt update
	
Add the official GPG key from the Docker repository to the system

	$ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

Add the Docker repository to APT sources

	$ sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

Update de database again

	$ sudo apt update

And finally install

	$ sudo apt-get install -y docker-engine

With docker installed enable the service

	$ sudo systemctl start docker
	
	$ sudo systemctl enable docker

Pull my image from the docker repository

	$ sudo docker pull molinex/laravel-angular-app

And run it 

	$ sudo docker run -p 8000:80 --name gitapp -d molinex/laravel-angular-app

Access the address http://localhost:8000 in your favorite browser and use the App

See results

![Screenshot](app.png)

### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com
	
	

