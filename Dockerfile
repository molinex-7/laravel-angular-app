# Using alpine linux because is a very small image and Distro
FROM alpine:3.5

# Install the packages
RUN apk --update add --no-cache nginx curl supervisor php7 php7-dom php7-fpm \
php7-mbstring php7-mcrypt php7-opcache php7-pdo php7-pdo_mysql \
php7-pdo_pgsql php7-pdo_sqlite php7-xml php7-phar php7-openssl php7-json \
php7-curl php7-ctype php7-session nodejs

# Clear cache
RUN rm -Rf /var/cache/apk/*

# Create a symlink to PHP7 as php
RUN ln -s /usr/bin/php7 /usr/bin/php

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# Install Angular
RUN npm install -g @angular/cli

# Copy config to our machine for nginx of image
COPY nginx.conf /etc/nginx/nginx.conf

# Copy config to our machine for supervisor of image
COPY supervisord.conf /etc/supervisord.conf

# Create a work dir
RUN mkdir -p /app

# Define that dir as work dir
WORKDIR /app

# Permissions
RUN chmod -R 755 /app

# Listen de ports
EXPOSE 80 443

# At last, starting this
CMD ["supervisord", "-c", "/etc/supervisord.conf"]

