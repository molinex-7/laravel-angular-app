// Our Angular App begins here
import { Component } from '@angular/core';
import { UserService } from './app.service';
import { User } from './../User';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Laravel 5 & Angular 4 App';
  _users: User[];

  constructor(private userv: UserService){ }

  onSubmit(form: NgForm): void {
    this.userv.getUser(JSON.stringify(form.value))
              .subscribe(
                (data: User[]) => this._users = data,
                error => console.log("Error :: " + error)
              );
  }
}
