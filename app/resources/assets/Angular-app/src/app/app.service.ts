// Our service to send request and map response
import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, Response } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from './../User';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {
  private _url = 'http://localhost:8000/api/user';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private _http: Http){}

  getUser(param: String): Observable<User[]> {
    return this._http.post(
        this._url,
        param,
        {headers: this.headers}
      ).map((response: Response) => {
        return <User[]>response.json();
      }).catch(this.handleError);
  }

  private handleError(error: Response){
    return Observable.throw(error.statusText);
  }
}
