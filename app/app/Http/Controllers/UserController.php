<?php
/*
* UserController
*
* Controller to receive request of our
* Angular App, and provide response
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\User;

class UserController extends Controller
{
  protected $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  public function user_data(Request $request)
  {
    $name = $request->get('name');
    $data = $this->user->find($name);

    return response($data)->header('Content-Type', 'application/json');
  }
}
