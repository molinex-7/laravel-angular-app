<?php
/**
 * GuzzleHttpRequest
 *
 * Class to instance GuzzleHttp\Client
 * and provide HTTP methods.
 *
 * PS: We only have the method get,
 * because that's what we need to do it.
 */

namespace App\Repositories;
use GuzzleHttp\Client;

class GuzzleHttpRequest
{
  protected $client;

  public function __construct(Client $client)
  {
    $this->client = $client;
  }

  public function get($url)
  {
    // Send a request to URI/posts
    $response = $this->client->request('GET', $url);

    return $response->getBody()->getContents();
  }
}
