<?php

/**
 * User
 *
 * Class extends GuzzleHttpRequest and
 * provide method to our Controller
 */

namespace App\Repositories;

class User extends GuzzleHttpRequest
{
  public function find($name)
  {
    return $this->get("/users/{$name}");
  }
}
